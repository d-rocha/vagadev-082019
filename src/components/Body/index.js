import React, {Component} from 'react';
import Recommendation from '../Recommendation';

import marioImg from '../../assets/images/Action-Figures---bombeiro-Mario-topzeira-das-galáxias.jpg';

import './style.scss';

export default class Body extends Component{
    constructor(props){
        super(props)
        this.state = {
            img: '',
            title: '',
            originalPrice: '',
            salePrice: ''
        }
    }

componentDidMount(){
    this.loadPage();
}

loadPage = () => {
    this.setState({
        img: marioImg,
        title: 'Action figure bombeiro Mario topzeira das galáxias ',
        originalPrice: 'de R$ 189,90',
        salePrice: 'R$ 149,90'
    })
}

    render(){

        return (
            <div>
                <div className="breadcrumb">
                    <nav>
                        <ul>
                            <li><p>N1</p></li>
                            <li>></li>
                            <li><p>action figures</p></li>
                            <li>></li>
                            <li><p className="sale">Super Mario</p></li>
                        </ul>
                    </nav>
                </div>
                <div className="firstContainer">
                    <div className="product">
                        <div className="marioImg">

                            <div className="smallImg">
                                <img src={this.state.img} alt="It's me Mario!" width="80px" height="80px"/>
                            </div>

                            <div className="bigImg">
                                <img src={this.state.img} alt="It's me Mario!" width="400px" height="400px"/>
                            </div>
                        </div>
                        <div className="detail">
                            <div className="titleInfo">
                                <span>{this.state.title}</span>
                            </div>
                            <div className="priceInfo">
                                <span>{this.state.originalPrice}</span>
                                <span>por {this.state.salePrice}</span>

                                <div>
                                    <button>COMPRA AE</button>
                                </div>
                            </div>
                            <div className="shippingInfo">
                                <label>CALCULE O FRETE</label>
                                    <div className="inputFields">
                                        <input type="text"  placeholder="00000" maxLength="5" className="firstInput" />
                                        <input type="text"  placeholder="000" maxLength="3" className="secondInput"/>
                                        <button>Calcular</button>
                                    </div>          
                            </div>
                        </div>
                    </div>
                </div>
                <div className="secondContainer">
                    <div className="description">
                        <p className="title">Descrição do Produto</p>

                        <p className="text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum 
                            facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure 
                            corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis. Lorem ipsum dolor sit amet, 
                            consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus 
                            ab mollitia esse odit cum! Corrupti et architecto facilis. 
                        </p>
                        <br/>
                        <p className="text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis 
                            et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis.
                        </p>
                    </div>
                </div>
                <Recommendation/>
            </div>
        );
    }
}