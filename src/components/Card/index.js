import React, {Component} from 'react';
import PropTypes from 'prop-types';

import imgDefault from '../../assets/images/Action-Figures---Super-Mario.jpg';

import './style.scss';

export default class Card extends Component{

    static defaultProps = {
        img: imgDefault,
        title: "Action Figures - Super Mario Bros - Bandai",
        originalPrice: "de R$ 189,90",
        salePrice: "por R$ 149,90",
    }

    static propTypes = {
        img: PropTypes.any.isRequired,
        title: PropTypes.string.isRequired,
        originalPrice: PropTypes.string.isRequired,
        salePrice: PropTypes.string.isRequired,
    };

    render(){
        return(
            <div className="containerCard">
                <div className="actionFigure">
                    <img src={this.props.img} alt="" />
                </div>
                <div className="title">
                    <span>{this.props.title}</span>
                </div>
                <div className="originalPrice">
                    <span>{this.props.originalPrice}</span>
                </div>
                <div className="salePrice">
                    <span>{this.props.salePrice}</span>
                </div>
            </div>

        );
    }
}
