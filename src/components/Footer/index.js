import React from 'react';

import logoFooter from '../../assets/images/logo-n1-white.png';

import './style.scss'

const Footer = () => (

    <footer>
        <div className="containerFooter">
            <div className="logoFooter">
                <img src={logoFooter} alt="" />
            </div>
            <div className="textFooter">
                <p>Agência N1 - Todos os direitos reservados</p>
            </div>
        </div>
    </footer>
);

export default Footer;