import React from 'react';
import Card from '../Card';

import strangerImg from '../../assets/images/action-figure-doctor-strange.jpg';
import ryuImg from '../../assets/images/figura-street-fighter-ryu.jpg';

import './style.scss';

const stranger = {
    img: strangerImg,
    title: "Action Figure Doctor Strange e Burning Flame Set - S.H.Figuarts",
    originalPrice: "de R$ 725,90",
    salePrice: "por R$ 624,90"
}

const ryu = {
    img: ryuImg,
    title: "Figura Street Fighter Ryu",
    originalPrice: "de R$ 5.500,20",
    salePrice: "por R$ 5.259,00"
}

const Recommendation = () => (

    <div className="card">
        <p className="titleCard">Quem viu, viu também</p>

        <div className="cardGroups">
            <Card img={stranger.img} title={stranger.title} originalPrice={stranger.originalPrice} salePrice={stranger.salePrice}/>
            <Card/>
            <Card img={ryu.img} title={ryu.title} originalPrice={ryu.originalPrice} salePrice={ryu.salePrice}/>
        </div>
    </div>
);

export default Recommendation;