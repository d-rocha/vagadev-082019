import React, {Component} from 'react';

import imgLogo from '../../assets/images/logo-n1.png';
import searchImg from '../../assets/search.svg';
import cartImg from '../../assets/shopping-cart.svg';

import './style.scss';

export default class Header extends Component{
    constructor(props){
        super(props)
        this.state = {
            counterItem: ''
        }
    }

    componentDidMount(){
        this.loadItemsCart();
    }

    loadItemsCart = () => {
        this.setState({ counterItem: 0 })
    }

    render(){
        return(
            <header>
                <div className="container">
                    <div className="menu">
                        <div className="logo">
                            <img src={imgLogo} alt="logo" />
                        </div>
                        <input type="checkbox" id="control-nav" />
                        <label for="control-nav" class="control-nav"></label>
                        <label for="control-nav" class="control-nav-close"></label>
                        <nav className="navHeader">
                            <ul>
                                <li><p className="link">Games</p></li>
                                <li><p className="link">Presentes</p></li>
                                <li><p className="link sale">SALE</p></li>
                            </ul>
                        </nav>
                        <div className="inputBox">
                            <input type="text" placeholder="Digite o que procura" />
                            
                            <img src={searchImg} alt="" className="search"/>
                        </div>
                        <div className="cartBox">
                            <img src={cartImg} alt="" className="cart" />

                            <span className="counterItemCart">{this.state.counterItem}</span>
                        </div>
                    </div>
                </div>        
            </header>
        );
    }
}